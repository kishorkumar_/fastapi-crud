from typing import List
from fastapi import FastAPI, Depends, Response, status, HTTPException
from sqlalchemy.orm import Session
from .import models, schemas, hashing

from .database import SessionLocal, engine
models.Base.metadata.create_all(engine)

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close


@app.post('/blog', status_code=status.HTTP_201_CREATED)
def create(req: schemas.Post, db: Session = Depends(get_db)):
    new_blog = models.Blog(title=req.title, body=req.body)
    db.add(new_blog)
    db.commit()
    db.refresh(new_blog)
    print(new_blog)
    return new_blog


# list for multiple data
@app.get('/blogs')
def blogs(db: Session = Depends(get_db)):
    blogs = db.query(models.Blog).all()
    return blogs


# @app.get('/blog/{id}', status_code=200)
# def blog(id,response:Response, db: Session = Depends(get_db)):
#     blog = db.query(models.Blog).filter(models.Blog.id == id).first()
#     if not blog:
#         response.status_code = status.HTTP_404_NOT_FOUND
#         return {"detail":f"Blog with id {id} is not available"}
#     return blog


@app.get('/blog/{id}', status_code=200, response_model=schemas.ShowBlog)
def blog(id, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id == id).first()
    if not blog:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"Blog with id {id} is not available")
        # response.status_code = status.HTTP_404_NOT_FOUND
        # return {"detail": f"Blog with id {id} is not available"}
    return blog


@app.delete('/blog/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete_blog(id, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id ==
                                        id)
    if not blog.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"blog with id {id} not found")
    blog.delete(synchronize_session=False)
    db.commit()
    return {"message": "Blog deleted successfully"}


@app.put('/blog/{id}', status_code=status.HTTP_202_ACCEPTED)
def update_blog(id, req: schemas.Post, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id == id)
    if not blog.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"blog with id {id} not found")

    print(req)
    blog.update({'title': req.title, 'body': req.body})
    # request.dict()
    db.commit()
    return 'updated'


@app.post('/signup', response_model=schemas.ShowUser)
def create_user(req: schemas.User, db: Session = Depends(get_db)):
    user = models.User(name=req.name, email=req.email,
                       password=hashing.Hash.bcrypt(req.password))
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


@app.get('/user/{id}', response_model=schemas.ShowUser)
def get_user(id, db: Session = Depends(get_db)):
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"user with id {id} not found")
    return user
