from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel
import uvicorn

app = FastAPI()


@app.get('/')
def index():
    return 'hey'


@app.get('/blogs')
def blogs(limit=10, published: bool = True, sort: Optional[str] = None):
    if published:
        return {'data': f"{limit} published blogs from db"}
    else:
        return {'data': f"{limit} blogs from db"}


@app.get('/blog/{id}')
def blog(id):
    return {'data': id}


class Blog(BaseModel):
    title: str
    body: str
    published: Optional[bool]


@app.post('/blog')
def create_blog(blog: Blog):
    return {'data': f"Blog is created with title as {blog.title}"}


if __name__ == "__main__":
    uvicorn.run(app,host="localhost",port=8082)